import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class PhotoService {

  apiUrl='https://jsonplaceholder.typicode.com/albums/1/photos';

  constructor(private http: HttpClient, private db: AngularFirestore) { }

  
}
