import { Component, OnInit } from '@angular/core';
import { Main } from '../interfaces/main';
import { MainService } from '../main.service';
import { Photos } from '../interfaces/photos';
import { ClassifiedService } from '../classified.service';

@Component({
  selector: 'app-mains',
  templateUrl: './mains.component.html',
  styleUrls: ['./mains.component.css']
})
export class MainsComponent implements OnInit {

  //geting posts from json not from db
  mains$:Main[];
  photos$:Photos[];
   
  //photosService: any;

  constructor(private mainService:MainService, public classifiedService:ClassifiedService) { }

  todoFunc(title:String,completed:boolean){
   
     this.classifiedService.addTodo(title,completed);
  }

  ngOnInit() {

    //geting posts from json
    this.mainService.getPhotos().subscribe(data => this.photos$ = data)
    this.mainService.getMains().subscribe(data => this.mains$ = data)

   

  }

}
