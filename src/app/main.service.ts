import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';
import { Main } from './interfaces/main';
import { Photos } from './interfaces/photos';


@Injectable({
  providedIn: 'root'
})
export class MainService {

  apiUrl='https://jsonplaceholder.typicode.com/posts/';
  apiurlphoto = "https://jsonplaceholder.typicode.com/todos"

  constructor(private http: HttpClient, private db: AngularFirestore) { }

  getMains(){
    return this.http.get<Main[]>(this.apiUrl)
  }

  getPhotos(){
    return this.http.get<Photos[]>(this.apiurlphoto);
  }
}
