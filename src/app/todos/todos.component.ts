import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ClassifiedService } from '../classified.service';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
  
  
})
export class TodosComponent implements OnInit {

  todos$:Observable<any[]>;

  constructor(private classifiedService:ClassifiedService) { }

  ngOnInit() {

    this.todos$ = this.classifiedService.getTodo();

  }

}
