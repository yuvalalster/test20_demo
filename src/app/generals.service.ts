import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { tap, catchError, map} from 'rxjs/operators';

 

@Injectable({
  providedIn: 'root'
})
export class GeneralsService {
 
  //generals: any =  [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}]

  userCollection:AngularFirestoreCollection = this.db.collection('users');
  generalCollection:AngularFirestoreCollection



  /*
  addGenerals(){
    setInterval(() => 
      this.books.push({title:'A new one', author:'New author'})
  , 5000);    
  }
  */

 
  /*
  getBooks(): any {
    const booksObservable = new Observable(observer => {
           setInterval(() => 
               observer.next(this.books)
           , 5000);
    });  
    return booksObservable;
  }
  */
  getGenerals(userId): Observable<any[]> {
    //const ref = this.db.collection('generals');
    //return ref.valueChanges({idField: 'id'});
    this.generalCollection = this.db.collection(`users/${userId}/generals`);
    console.log('Generals collection created');
    return this.generalCollection.snapshotChanges().pipe(
      map(collection => collection.map(document => {
        const data = document.payload.doc.data();
        data.id = document.payload.doc.id;
        return data;
      }))
    );    
  } 


  getGeneral(userId, id:string):Observable<any>{
    return this.db.doc(`users/${userId}/generals/${id}`).get();
  }
  
  addGeneral(userId:string, title:string, date:Date,room:string){
    console.log('In add generals');
    const general = {title:title,date:date,room:room}
    //this.db.collection('books').add(book)  
    this.userCollection.doc(userId).collection('generals').add(general);
  } 

  updateGeneral(userId:string, id:string,title:string,date:Date,room:string){
    this.db.doc(`users/${userId}/generals/${id}`).update(
      {
        title:title,
        date:date,
        room:room
      }
    )
  }
  
  deleteGeneral(userId:string, id:string){
    this.db.doc(`users/${userId}/generals/${id}`).delete();
  }

  constructor(private db: AngularFirestore,
              private authService:AuthService) {}
}