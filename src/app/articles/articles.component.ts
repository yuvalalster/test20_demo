import { Component, OnInit } from '@angular/core';
import { ClassifiedService } from '../classified.service';
import { Observable } from 'rxjs/internal/Observable';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit {

  deleteArticle(id:string){
    this.classifiedService.deleteArticle(id);
  }

  articles$:Observable<any[]>;

  constructor(private classifiedService:ClassifiedService, public authService:AuthService) { }

  ngOnInit() {

    this.articles$ = this.classifiedService.getArticles();

  }

}
